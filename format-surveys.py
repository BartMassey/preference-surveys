#!/usr/bin/python3
# Copyright (c) 2018 Bart Massey
# [This program is licensed under the "MIT License"]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


import csv
import sys
import re

badchar = re.compile("[^a-z_]")
stripmiddle = re.compile("_.*_")

indexf = open("responses/index.mdwn", "w")
print("# Project Preference Surveys", file=indexf)
print(file=indexf)

r = csv.reader(sys.stdin)
headers = next(r)
projects = [h[2:-1] for h in headers[3:-1]]
responses = []
for s in r:
    name = s[2].strip()
    uname = name.replace(" ", "_").lower()
    uname = badchar.sub("", uname)
    uname = stripmiddle.sub("_", uname)
    last = name.lower().split()[-1]
    responses.append(((last, uname, name), s))

for t, s in sorted(responses):
    _, uname, name = t
    print("* [[" + name.replace(" ", "_") + "|" + uname + "]]", file=indexf)
    with open("responses/" + uname + ".mdwn", "w") as f:
        print("# Project Preference Survey:", name, file=f)
        print(file=f)
        rs = [(int(n), projects[int(n) - 1]) for n in s[3:-1]]
        for i, p in sorted(rs):
            print(str(i) + ".", p, file=f)
        print(file=f)
        print(s[-1], file=f)
