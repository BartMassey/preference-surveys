# PSU CS Capstone Preference Survey Processing
Copyright (c) 2019 Bart Massey

This is a random blob of code used to process Portland State
Unversity Computer Science Capstone "Project Preference
Survey" forms as taken through Google Forms. I should
probably include the Form as well.

# License

This program is licensed under the "MIT License". Please see
the file `LICENSE` in this distribution for license terms.
